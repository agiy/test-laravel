<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        \App\User::create([
            'name'     => 'test kun',
            'email'    => "test@test.jp",
            'password' => bcrypt('password'),
        ]);

        \App\User::create([
            'name'     => 'test kun',
            'email'    => "test2@test.jp",
            'password' => bcrypt('password'),
        ]);
    }
}
