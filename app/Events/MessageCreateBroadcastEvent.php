<?php
namespace App\Events;
use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageCreateBroadcastEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
    public $message;


    public function __construct(string $text)
    {
        $this->message = ['text' => $text];
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('meet.' . 1);
    }
}