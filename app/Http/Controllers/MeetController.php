<?php

namespace App\Http\Controllers;

use App\Events\MessageCreateBroadcastEvent;
use Illuminate\Http\Request;
class MeetController extends Controller
{

    public function index()
    {
        return view('meet');
    }

    public function createMessage(Request $request)
    {
        $params = $request->all();

        event(new MessageCreateBroadcastEvent($params['text']));

        return json_encode(['OK']);
    }
}