// import 'onsenui/css/onsenui.css';
// import 'onsenui/css/onsen-css-components.css';
// import VueOnsen from 'vue-onsenui'; // This already imports 'onsenui'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router';
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// Vue.use(VueOnsen);
Vue.use(Vuetify);
Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('main-vue', require('./components/Main.vue'));
Vue.component('toolbar', require('./components/Toolbar.vue'));
Vue.component('top-page', require('./components/TopPage.vue'));

Vue.component('meet', require('./components/Meet.vue'));

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/home',  name: 'home',component :require('./components/ModalTest.vue') },
    ]
})

const app = new Vue({
    router,
    el: '#app'
});
