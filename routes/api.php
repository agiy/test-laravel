<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware(['api'])->group(function () {
    Route::get('/', function () {
        return json_encode(['普通のapi']);
    });

    Route::post('login', 'Api\AuthController@authenticate');

    Route::middleware(['jwt.auth'])->group(function () {
        Route::get('user', 'Api\AuthController@getUser');
    });
});